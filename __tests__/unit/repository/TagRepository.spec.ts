import {TagRepository} from "../../../src/repository/TagRepository";
import {DatabaseClient} from "../../../src/repository/DatabaseClient";
import {DataMapper, ScanIterator} from "@aws/dynamodb-data-mapper";
import {TagBuilder} from "../domain/TagBuilder";
import {TagEntityBuilder} from "../domain/db/TagEntityBuilder";
import {TagType} from "../../../src/domain/TagType";
import {TagEntity} from "../../../src/domain/entity/TagEntity";

var jestMock = require("jest-mock");

jest.mock('../../../src/repository/DatabaseClient');
jest.mock('@aws/dynamodb-data-mapper');

const DatabaseClientMockType = <jest.Mock<DatabaseClient>>DatabaseClient;
const DataMapperMockedType = <jest.Mock<DataMapper>>DataMapper;

const mockedDatabaseClient = new DatabaseClientMockType();
const mockedDataMapper = new DataMapperMockedType();

const tagRepository = new TagRepository(mockedDatabaseClient);

beforeEach(() => {
    (mockedDatabaseClient.getDataMapper as jest.Mock).mockClear();
    (mockedDataMapper.put as jest.Mock).mockClear();
    (mockedDataMapper.scan as jest.Mock).mockClear();

    (mockedDatabaseClient.getDataMapper as jest.Mock).mockReturnValue(mockedDataMapper);
});

test('Calling getTags retrieves all the tags from the database', async () => {
    const mockedTagEntity1 = new TagEntityBuilder().withName('Cannibal Corpse').withType(TagType.BAND.toString()).withReviewIds(['ReviewId-234']).build();
    const mockedTagEntity2 = new TagEntityBuilder().withName('Black Metal').withType(TagType.GENRE.toString()).withReviewIds(['ReviewId-123']).build();

    const expectedTag1 = new TagBuilder().withName('Cannibal Corpse').withType(TagType.BAND).withReviewIds(['ReviewId-234']).build();
    const expectedTag2 = new TagBuilder().withName('Black Metal').withType(TagType.GENRE).withReviewIds(['ReviewId-123']).build();

    (mockedDataMapper.scan as jest.Mock).mockReturnValue(buildMockedScanIterator([mockedTagEntity1, mockedTagEntity2]));

    await expect(tagRepository.getTags()).resolves.toEqual([expectedTag1, expectedTag2]);

    expect(mockedDataMapper.scan).toHaveBeenCalledWith(TagEntity);

});

test('Calling getTagsById retrieves all matching tags from the database', async() => {
    const mockedTagEntity1 = new TagEntityBuilder().withName('Slayer').withType(TagType.BAND.toString()).withReviewIds(['ReviewId-234']).build();
    const mockedTagEntity2 = new TagEntityBuilder().withName('Doom Metal').withType(TagType.GENRE.toString()).withReviewIds(['ReviewId-123']).build();
    const expectedTagEntity1 = new TagEntity('Slayer');
    const expectedTagEntity2 = new TagEntity('Doom Metal');

    const expectedTag1 = new TagBuilder().withName('Slayer').withType(TagType.BAND).withReviewIds(['ReviewId-234']).build();
    const expectedTag2 = new TagBuilder().withName('Doom Metal').withType(TagType.GENRE).withReviewIds(['ReviewId-123']).build();

    (mockedDataMapper.batchGet as jest.Mock).mockReturnValue([mockedTagEntity1, mockedTagEntity2])

    await expect(tagRepository.getTagsById(['Slayer', 'Doom Metal'])).resolves.toEqual([expectedTag1, expectedTag2]);

    expect(mockedDataMapper.batchGet).toHaveBeenCalledWith([expectedTagEntity1, expectedTagEntity2])
});

test('Calling writeTags writes all tags to the database', async() => {
    const inputTag1 = new TagBuilder().withName('Pallbearer').withType(TagType.BAND).withReviewIds(['ReviewId-234']).build();
    const inputTag2 = new TagBuilder().withName('Power Metal').withType(TagType.GENRE).withReviewIds(['ReviewId-123']).build();

    const expectedTagEntity1 = new TagEntityBuilder().withName('Pallbearer').withType(TagType.BAND.toString()).withReviewIds(['ReviewId-234']).build();
    const expectedTagEntity2 = new TagEntityBuilder().withName('Power Metal').withType(TagType.GENRE.toString()).withReviewIds(['ReviewId-123']).build();

    const mockedTagEntity1 = new TagEntityBuilder().withName('Pallbearer').withType(TagType.BAND.toString()).withReviewIds(['ReviewId-234']).build();
    const mockedTagEntity2 = new TagEntityBuilder().withName('Power Metal').withType(TagType.GENRE.toString()).withReviewIds(['ReviewId-123']).build();

    const expectedTag1 = new TagBuilder().withName('Pallbearer').withType(TagType.BAND).withReviewIds(['ReviewId-234']).build();
    const expectedTag2 = new TagBuilder().withName('Power Metal').withType(TagType.GENRE).withReviewIds(['ReviewId-123']).build();

    (mockedDataMapper.batchPut as jest.Mock).mockReturnValue([mockedTagEntity1, mockedTagEntity2])

    await expect(tagRepository.writeTags([inputTag1, inputTag2])).resolves.toEqual([expectedTag1, expectedTag2]);

    expect(mockedDataMapper.batchPut).toHaveBeenCalledWith([expectedTagEntity1, expectedTagEntity2]);
});

test('Calling deleteTag deletes the tag from the database', async() => {
    const expectedTagEntity = new TagEntity('SomeTag');

    await tagRepository.deleteTag('SomeTag');
    expect(mockedDataMapper.delete).toHaveBeenCalledWith(expectedTagEntity);
});

function buildMockedScanIterator(mockedTagEntities : TagEntity[]) {
    const ScanIteratorMockType = jestMock.generateFromMetadata(jestMock.getMetadata(ScanIterator));
    const mockedScanResults = new ScanIteratorMockType();
    mockedScanResults[Symbol.iterator] = jest.fn(() => mockedTagEntities.values());
    return mockedScanResults;
}