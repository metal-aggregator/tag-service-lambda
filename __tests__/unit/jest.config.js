module.exports = {
  name: 'unit',
  displayName: 'Unit Tests',

  "testMatch": [
    "<rootDir>/**/*.spec.ts"
  ],
  "transform": {
    "^.+\\.(ts|tsx)$": "ts-jest"
  }
}
