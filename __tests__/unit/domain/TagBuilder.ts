import {TagType} from "../../../src/domain/TagType";
import {Tag} from "../../../src/domain/Tag";

export class TagBuilder {
    private _name = 'Death Metal';
    private _type = TagType.GENRE;
    private _reviewIds = ['ReviewId-123'];

    withName(name: string) {
        this._name = name;
        return this;
    }

    withType(type: TagType) {
        this._type = TagType[<keyof typeof TagType> type.toString()];
        return this;
    }

    withReviewIds(reviewIds: string[]) {
        this._reviewIds = reviewIds;
        return this;
    }

    build() {
        return new Tag(this._name, this._type, this._reviewIds);
    }

}