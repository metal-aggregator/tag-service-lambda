import {TagEntity} from "../../../../src/domain/entity/TagEntity";

export class TagEntityBuilder {
    private _name = 'Death Metal';
    private _type = 'GENRE';
    private _reviewIds = ['ReviewId-123'];

    withName(name: string) {
        this._name = name;
        return this;
    }

    withType(type: string) {
        this._type = type;
        return this;
    }

    withReviewIds(reviewIds: string[]) {
        this._reviewIds = reviewIds;
        return this;
    }

    build() {
        const tagEntity = new TagEntity();
        tagEntity.name = this._name;
        tagEntity.type = this._type;
        tagEntity.reviewIds = this._reviewIds;
        return tagEntity;
    }
}