import {TagRepository} from "../../../src/repository/TagRepository";
import {TagService} from "../../../src/service/TagService";
import {TagBuilder} from "../domain/TagBuilder";
import {TagType} from "../../../src/domain/TagType";

jest.mock('../../../src/repository/TagRepository');
const TagRepositoryMockType = <jest.Mock<TagRepository>>TagRepository;
const mockedTagRepository = new TagRepositoryMockType();

const tagService = new TagService(mockedTagRepository);

beforeEach(() => {
    (mockedTagRepository.getTags as jest.Mock).mockClear();
});

test('Calling getTags() correctly invokes the repository and returns the expected tags', async () => {

    const mockedTag = new TagBuilder().build();
    const expectedTag = new TagBuilder().build();

    (mockedTagRepository.getTags as jest.Mock).mockResolvedValue([mockedTag]);

    await expect(tagService.getTags()).resolves.toEqual([expectedTag]);

    expect(mockedTagRepository.getTags).toHaveBeenCalledWith();

});

test('Calling createTags() correctly interacts with the TagRepository to filter and apply the reviewId to all input tags, creating new tags as needed', async () => {

    const inputTags = ['X', 'Y', 'Z'];
    const inputReviewId = 'reviewId-123';

    const expectedProposedTags = ['X', 'Y', 'Z'];
    const mockedExistingTags = [
        new TagBuilder().withName('Y').withType(TagType.UNKNOWN).withReviewIds([]).build(),
        new TagBuilder().withName('Z').withType(TagType.GENRE).withReviewIds(['reviewId-456', 'reviewId-123']).build()
    ];

    const expectedRepositoryTags = [
        new TagBuilder().withName('Y').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
        new TagBuilder().withName('Z').withType(TagType.GENRE).withReviewIds(['reviewId-456', 'reviewId-123']).build(),
        new TagBuilder().withName('X').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build()
    ];

    const mockedRepositoryTags = [
        new TagBuilder().withName('X').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
        new TagBuilder().withName('Y').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
        new TagBuilder().withName('Z').withType(TagType.GENRE).withReviewIds(['reviewId-123', 'reviewId-456']).build()
    ];

    const expectedFinalTags = [
        new TagBuilder().withName('X').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
        new TagBuilder().withName('Y').withType(TagType.UNKNOWN).withReviewIds(['reviewId-123']).build(),
        new TagBuilder().withName('Z').withType(TagType.GENRE).withReviewIds(['reviewId-123', 'reviewId-456']).build()
    ];

    (mockedTagRepository.getTagsById as jest.Mock).mockResolvedValue(mockedExistingTags);
    (mockedTagRepository.writeTags as jest.Mock).mockResolvedValue(mockedRepositoryTags);

    await expect(tagService.createTags(inputTags, inputReviewId)).resolves.toEqual(expect.arrayContaining(expectedFinalTags));

    expect(mockedTagRepository.getTagsById).toHaveBeenCalledWith(expectedProposedTags);
    expect(mockedTagRepository.writeTags).toHaveBeenCalledWith(expectedRepositoryTags);

});

test('Calling deleteTag correctly interacts with the repository', async() => {
    await tagService.deleteTag('SomeTag');
    expect(mockedTagRepository.deleteTag).toHaveBeenCalledWith('SomeTag');
});