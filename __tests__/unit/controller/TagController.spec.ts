import {TagService} from "../../../src/service/TagService";
import {TagController} from "../../../src/controller/TagController";
import {TagBuilder} from "../domain/TagBuilder";
import {TagType} from "../../../src/domain/TagType";
import {TagDTO} from "../../../src/domain/dto/TagDTO";
import {TagsDTO} from "../../../src/domain/dto/TagsDTO";
import {Request, Response} from "express";

jest.mock('../../../src/service/TagService');
const TagServiceMockType = <jest.Mock<TagService>>TagService;
const mockedTagService = new TagServiceMockType();

const tagController = new TagController(mockedTagService);

test('Calling getTags correctly invokes the TagService and returns the expected tags', async() => {
    const mockedTag1 = new TagBuilder().withType(TagType.OTHER).withName('Victory Records').withReviewIds(['id1', 'id2']).build();
    const mockedTag2 = new TagBuilder().withType(TagType.GENRE).withName('Power Metal').withReviewIds(['id1', 'id3', 'id4']).build();

    const expectedFinalTags = new TagsDTO([
        new TagDTO(new TagBuilder().withType(TagType.OTHER).withName('Victory Records').withReviewIds(['id1', 'id2']).build()),
        new TagDTO(new TagBuilder().withType(TagType.GENRE).withName('Power Metal').withReviewIds(['id1', 'id3', 'id4']).build())
    ]);

    (mockedTagService.getTags as jest.Mock).mockResolvedValue([mockedTag1, mockedTag2]);

    await expect(tagController.getTags()).resolves.toEqual(expectedFinalTags);

    expect(mockedTagService.getTags).toHaveBeenCalled();

});

test('Calling createTags correctly invokes the TagService and returns the expected tags', async() => {

    const mockedTag1 = new TagBuilder().withType(TagType.BAND).withName('Watain').withReviewIds(['id1', 'id2']).build();
    const mockedTag2 = new TagBuilder().withType(TagType.GENRE).withName('Post Metal').withReviewIds(['id1', 'id3', 'id4']).build();

    const expectedFinalTags = new TagsDTO([
        new TagDTO(new TagBuilder().withType(TagType.BAND).withName('Watain').withReviewIds(['id1', 'id2']).build()),
        new TagDTO(new TagBuilder().withType(TagType.GENRE).withName('Post Metal').withReviewIds(['id1', 'id3', 'id4']).build())
    ]);

    const inputRequest = {
        body: {
            tags: ['Watain', 'Post Metal'],
            reviewId: 'id123'
        },
    } as Request;

    (mockedTagService.createTags as jest.Mock).mockResolvedValue([mockedTag1, mockedTag2]);

    await expect(tagController.createTags(inputRequest)).resolves.toEqual(expectedFinalTags);

    expect(mockedTagService.createTags).toHaveBeenCalledWith(['Watain', 'Post Metal'], 'id123');

});

test('Calling deleteTag correctly invokes the TagService and sets the response to a 204 status', async() => {

    const mockedResponse = {
        sendStatus: jest.fn()
    } as unknown as Response;

    (mockedTagService.deleteTag as jest.Mock).mockResolvedValue(null);

    await tagController.deleteTag('SomeTag', mockedResponse);

    expect(mockedResponse.sendStatus).toHaveBeenCalledWith(204);

});