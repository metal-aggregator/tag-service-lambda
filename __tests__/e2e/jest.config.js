module.exports = {
  name: 'e2e',
  displayName: 'End-to-End Tests',

  "testMatch": [
    "<rootDir>/**/*.spec.ts"
  ],
  "transform": {
    "^.+\\.(ts|tsx)$": "ts-jest"
  }
}
