const axios = require('axios');

// Move this to a common config
axios.interceptors.request.use((request: any) => {
    console.log('Outgoing request started: ', JSON.stringify(request));
    return request;
});
axios.interceptors.response.use((response: any) => {
    console.log('Outgoing request finished: ', JSON.stringify(response));
    return response;
});

test('Calling /tags returns a 200 with some well-formed tags', async() => {
    const response = await axios.get('http://localhost:3000/tags');

    expect(response.status).toEqual(200);

    const responseBody = response.data;
    expect(responseBody.tags).toBeTruthy();
    expect(responseBody.tags.length).toBeGreaterThan(1);
    responseBody.tags.every((tag: any) => {
        expect(tag.name).toBeTruthy();
        expect(tag.type).toBeTruthy();
        expect(tag.reviewIds).toBeTruthy();
        expect(Object.keys(tag).length).toEqual(3);
    });
});

describe('POST /tags', () => {
    let tagsToDelete : string[] = [];

    test('Calling POST /tags successfully creates a tag with the specified review id', async() => {
        const response = await axios.post('http://localhost:3000/tags',
            {
                'tags': [
                    'Doom Metal',
                    'Stratovarius'
                ],
                'reviewId': 'ReviewId-12345'
            });
        expect(response.status).toEqual(200);

        const responseBody = response.data;
        expect(responseBody.tags).toBeTruthy();
        tagsToDelete = responseBody.tags.map((tag: any) => tag.name);
        expect(responseBody.tags.length).toEqual(2);

        const expectedTags = ['Doom Metal', 'Stratovarius']
        expectedTags.forEach(expectedTag => {
            const actualTag = responseBody.tags.find((candidate: any) => candidate.name == expectedTag);
            expect(actualTag).toBeTruthy();
            expect(actualTag.reviewIds).toEqual(['ReviewId-12345']);
            expect(actualTag.type).toEqual('UNKNOWN');
        });

    });

    afterEach(async() => {
        for (const tagName of tagsToDelete) {
            await axios.delete(`http://localhost:3000/tags/${tagName}`);
        }
    });

});