import "reflect-metadata";
import logger from 'morgan';
import {InversifyExpressServer} from "inversify-express-utils";
import {Container} from "inversify";
import * as bodyParser from "body-parser";

const AWS = require("aws-sdk");

import "./src/controller/TagController";
import {TagService} from "./src/service/TagService";
import {TagRepository} from "./src/repository/TagRepository";
import {DatabaseClient} from "./src/repository/DatabaseClient";

AWS.config.update({
    region: "us-east-2",
    endpoint: "http://localhost:8000"
});

const container = new Container();
container.bind<TagRepository>("TagRepository").to(TagRepository).inSingletonScope();
container.bind<TagService>("TagService").to(TagService).inSingletonScope();
container.bind<DatabaseClient>("DatabaseClient").to(DatabaseClient).inSingletonScope();

let server = new InversifyExpressServer(container);
server.setConfig((app) => {
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(logger('dev'));

    app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PATCH, PUT');
        next();
    });
});

const app = server.build();

app.listen(3000);

