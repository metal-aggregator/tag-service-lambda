import {TagEntity} from "../domain/entity/TagEntity";
import {Tag} from "../domain/Tag";
import {inject, injectable as Injectable} from "inversify";
import {DatabaseClient} from "./DatabaseClient";
import {ScanIterator} from "@aws/dynamodb-data-mapper";

@Injectable()
export class TagRepository {

    public constructor(@inject("DatabaseClient") private databaseClient: DatabaseClient) { }

    public async getTags() : Promise<Tag[]> {
        const allTagEntities = this.databaseClient.getDataMapper().scan(TagEntity);
        return this.mapIteratorEntitiesToDomain(allTagEntities);
    }

    public async getTagsById(tags: string[]) : Promise<Tag[]> {
        const inputTagEntities = tags.map(tagName => new TagEntity(tagName));
        const tagEntities = this.databaseClient.getDataMapper().batchGet(inputTagEntities);
        return this.mapAsyncIteratorEntitiesToDomain(tagEntities);
    }

    public async writeTags(tags: Tag[]) : Promise<Tag[]> {
        const tagEntities = tags.map(tag => TagEntity.fromDomain(tag));
        const writtenEntities = this.databaseClient.getDataMapper().batchPut(tagEntities)
        return this.mapAsyncIteratorEntitiesToDomain(writtenEntities);
    }

    public async deleteTag(tagName: string) : Promise<void> {
        const tagEntity = new TagEntity(tagName);
        await this.databaseClient.getDataMapper().delete(tagEntity);
    }

    private async mapIteratorEntitiesToDomain(tagEntities : ScanIterator<TagEntity>) {
        const tags : Array<Tag> = [];
        for await (const tagEntity of tagEntities) {
            tags.push(tagEntity.toDomain());
        }
        return tags;
    }

    private async mapAsyncIteratorEntitiesToDomain(tagEntities : AsyncIterableIterator<TagEntity>) {
        const tags : Array<Tag> = [];
        for await (const tagEntity of tagEntities) {
            tags.push(tagEntity.toDomain());
        }
        return tags;
    }

}
