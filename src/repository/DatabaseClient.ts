import {injectable} from "inversify";
import {DataMapper} from "@aws/dynamodb-data-mapper";
import DynamoDB = require("aws-sdk/clients/dynamodb");
const AWS = require("aws-sdk");

@injectable()
export class DatabaseClient {

    private client = new DynamoDB({region: 'us-west-2'});
    private mapper = new DataMapper({client: this.client});

    constructor() {
        AWS.config.update({
            region: "us-east-2",
            endpoint: "http://localhost:8000"
        });
    }

    public getDataMapper() : DataMapper {
        return this.mapper;
    }

}