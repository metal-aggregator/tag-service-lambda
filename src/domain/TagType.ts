export enum TagType {
    GENRE = 'GENRE',
    BAND = 'BAND',
    SCORE = 'SCORE',
    UNKNOWN = 'UNKNOWN',
    OTHER = 'OTHER'
}