import {Tag} from '../Tag';
import {TagType} from "../TagType";

export class TagDTO {
    name: string;
    type: string;
    reviewIds: string[];

    constructor(tag : Tag) {
        this.name = tag.name;
        this.type = TagType[tag.type];
        this.reviewIds = tag.reviewIds;
    }
}