export class TagsCreateRequestDTO {

    constructor(private _tags: string[], private _reviewId: string) { }

    get tags(): string[] {
        return this._tags;
    }

    get reviewId(): string {
        return this._reviewId;
    }
}