import {TagType} from "../TagType";
import {Tag} from "../Tag";
import {attribute, hashKey, table} from "@aws/dynamodb-data-mapper-annotations";

@table('Tags')
export class TagEntity {

    @hashKey({attributeName: 'tag_name'})
    name?: string;

    @attribute()
    type?: string;

    @attribute()
    reviewIds?: string[];

    constructor(name?: string) {
        this.name = name;
    }

    static fromDomain(tag: Tag) : TagEntity {
        const entity = new TagEntity();
        entity.name = tag.name;
        entity.type = tag.type.toString();
        entity.reviewIds = tag.reviewIds;
        return entity;
    }

    toDomain() : Tag {
        return new Tag(this.name || '', TagType[<keyof typeof TagType> this.type], this.reviewIds || [])
    }

}