import {TagType} from "./TagType";

export class Tag {

    constructor(private _name: string, private _type: TagType = TagType.UNKNOWN, private _reviewIds: string[] = []) {}

    get name(): string {
        return this._name;
    }

    get type(): TagType {
        return this._type;
    }

    get reviewIds(): string[] {
        return this._reviewIds;
    }

    public addReviewId(reviewId : string): void {
        if (!this._reviewIds.includes(reviewId)) {
            this._reviewIds.push(reviewId);
        }
    }
}