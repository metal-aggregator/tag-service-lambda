import {inject} from "inversify";
import {TagService} from "../service/TagService";
import * as express from "express";
import {controller, httpDelete, httpGet, httpPost, request, requestParam, response} from "inversify-express-utils";
import {TagDTO} from "../domain/dto/TagDTO";
import {TagsDTO} from "../domain/dto/TagsDTO";
import {TagsCreateRequestDTO} from "../domain/dto/TagsCreateRequestDTO";
import {Tag} from "../domain/Tag";

@controller('/tags')
export class TagController {

    constructor(@inject("TagService") private tagService: TagService) {}

    @httpGet("/")
    public async getTags() : Promise<TagsDTO> {
        const tags = await this.tagService.getTags();
        return new TagsDTO(this.mapToDtos(tags));
    }

    @httpPost("/")
    public async createTags(@request() req: express.Request<{}, {}, TagsCreateRequestDTO>) : Promise<TagsDTO> {
        const tags = await this.tagService.createTags(req.body.tags, req.body.reviewId)
        return new TagsDTO(this.mapToDtos(tags));
    }

    @httpDelete("/:tagName")
    public async deleteTag(@requestParam("tagName") tagName : string, @response() res: express.Response) : Promise<void> {
        await this.tagService.deleteTag(tagName).then(() => res.sendStatus(204));
    }

    private mapToDtos(tags : Tag[]) : TagDTO[] {
        return tags.map(tag => new TagDTO(tag));
    }


}