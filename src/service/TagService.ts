import {Tag} from "../domain/Tag";
import {inject, injectable} from "inversify";
import {TagRepository} from "../repository/TagRepository";

@injectable()
export class TagService {

    public constructor(@inject("TagRepository") private tagRepository: TagRepository) { }

    public async getTags() : Promise<Tag[]> {
        return await this.tagRepository.getTags();
    }

    public async createTags(tags: string[], reviewId: string) : Promise<Tag[]> {
        const existingTags = await this.tagRepository.getTagsById(tags);
        const newTags = this.determineNewTags(tags, existingTags);
        const allTagsToWrite = [...existingTags, ...newTags];
        allTagsToWrite.forEach(tag => tag.addReviewId(reviewId));
        return await this.tagRepository.writeTags(allTagsToWrite);
    }

    public async deleteTag(tagName: string) : Promise<void> {
        await this.tagRepository.deleteTag(tagName);
    }

    private determineNewTags(proposedTags: string[], existingTags: Tag[]) : Tag[] {
        const existingTagNames = existingTags.map(tag => tag.name);
        return proposedTags.filter(proposedTag => !existingTagNames.includes(proposedTag)).map(proposedTag => new Tag(proposedTag));
    }

}
